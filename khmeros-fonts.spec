Name:           khmeros-fonts
Version:        5.0
Release:        27
Summary:        Khmer font set. 
License:        LGPLv2+
URL:            http://www.khmeros.info/en/fonts
Source0:        https://nchc.dl.sourceforge.net/project/khmer/Fonts%20-%20KhmerOS/KhmerOS%20Fonts%205.0-%20LGPL%20Licence/All_KhmerOS_5.0.zip
Source1:        License

BuildArch:      noarch
BuildRequires:  fontpackages-devel
Requires:       fontpackages-filesystem

%description
All font include Khmer and Latin alphabets, and they have equivalent sizes for
Khmer and English alphabets, so that when texts mix both it is not necessary to
have different size points of the text for each language. This makes
localization possible.

%prep
%autosetup -n All_KhmerOS_5.0 -p1
install -p %{SOURCE1} .

%build

%install
mv 'KhmerOS .ttf' KhmerOS.ttf
install -m 0755 -d %{buildroot}%{_fontdir}
install -m 0644 -p *.ttf %{buildroot}%{_fontdir}

%files
%license License
%{_fontdir}/*.ttf

%changelog
* Sat Nov 30 2019 jiaxiya <jiaxiyajiaxiya@163.com> - 5.0-27
- Package init
